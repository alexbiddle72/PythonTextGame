class FirstClass():
	def __init__(self,name,job):
		self.name = name
		self.job = job
	
class SecondClass(FirstClass): #inherit from first class
	def __init__(self,name,job):
		self.name = name
		self.job = job
	
	def printJob(self):
		print "My job is " + self.job

class ThirdClass(FirstClass):
	def __init__(self,name,job):
		self.name = name
		self.job = job
	def printName(self):
		print "My name is" + self.name + " And my job is " + self.job 
		
AlexBiddle = FirstClass("Alex", "Doctor")

StevenOwatemi = SecondClass("Steven","Lawyer")

NaheedaBegum = ThirdClass("Naheeda", "Artist")

StevenOwatemi.printJob()

NaheedaBegum.printName()

